import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Layout from './HOC/Layout';
import Home from './components/Home/Home';
import Auth from './HOC/auth';
import Login from './containers/Admin/Login';
import Logout from './components/Admin/Logout';
import Register from './containers/Admin/Register';
import User from './components/Admin/User';
import AddStand from './containers/Admin/AddStand';
import EditStand from './containers/Admin/EditStand';
import UserStands from './components/Admin/UserStands';

const Routes = () => {
    return (
        <Layout>
            <Switch>
                <Route exact path="/" component={Auth(Home, null)} />
                <Route exact path="/login" component={Auth(Login, false)} />
                <Route exact path="/user/logout" component={Auth(Logout, true)} />
                <Route exact path="/user/add" component={Auth(AddStand, true)} />
                <Route exact path="/user/edit-stand/:id" component={Auth(EditStand, true)} />
                <Route exact path="/user" component={Auth(User, true)} />
                <Route exact path="/user/user-stands" component={Auth(UserStands, true)} />
                {/* <Route exact path="/user/register" component={Auth(Register, true)} /> */}
            </Switch>
        </Layout>
    )
}

export default Routes;