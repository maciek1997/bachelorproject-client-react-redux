import React from 'react'

class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            // liczba aktywnych sugestii
            activeSuggestion: 0,
            // tablica sugestii, które pasują do tego co wpisał użytkownik
            filteredSuggestions: [],
            // jeśli tablica jest pusta, to nie wyświetlamy jej
            showSuggestions: false,
            // to co wpisuje użytkownik
            userInput: ""
        };
    }

    // na każdą zmianę inputa w formularzu jest wykonywana ta funkcja
    onChange = e => {
        const userInput = e.currentTarget.value;

        const { suggestions } = this.props;

        const suggestions2 = suggestions.map(item => {
            return item.name
        })

        // filtrowanie podpowiedzi
        const filteredSuggestions = suggestions2.filter(
            suggestion =>
                suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
        );

        // robi update state i wyświetlanej wartości
        this.setState({
            activeSuggestion: 0,
            filteredSuggestions,
            showSuggestions: true,
            userInput: e.currentTarget.value
        });
    };

    // event odpalany na kliknięcie użytkownika w sugestie
    onClick = e => {
        this.setState({
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: e.currentTarget.innerText
        });
        const item = this.props.suggestions.find(item => item.name === e.currentTarget.innerText);
        this.props.method(item);
    };

    // kliknięcie w przycisk klawiatury
    onKeyDown = e => {
        const { activeSuggestion, filteredSuggestions } = this.state;

        // przycisk enter
        if (e.keyCode === 13) {
            this.setState({
                activeSuggestion: 0,
                showSuggestions: false,
                userInput: filteredSuggestions[activeSuggestion]
            });
            const item = this.props.suggestions.find(item => item.name === filteredSuggestions[activeSuggestion]);
            this.props.method(item);
        }
        // strzałka w dół
        else if (e.keyCode === 38) {
            if (activeSuggestion === 0) {
                return;
            }

            this.setState({ activeSuggestion: activeSuggestion - 1 });
        }
        // strzałka w dół
        else if (e.keyCode === 40) {
            if (activeSuggestion - 1 === filteredSuggestions.length) {
                return;
            }

            this.setState({ activeSuggestion: activeSuggestion + 1 });
        }
    };

    render() {
        const {
            onChange,
            onClick,
            onKeyDown,
            state: {
                activeSuggestion,
                filteredSuggestions,
                showSuggestions,
                userInput
            }
        } = this;

        let suggestionsListComponent;

        if (showSuggestions && userInput) {
            if (filteredSuggestions.length) {
                suggestionsListComponent = (
                    <ul className="suggestions">
                        {filteredSuggestions.map((suggestion, index) => {
                            let className;

                            // aktywna sugestia podświetlona
                            if (index === activeSuggestion) {
                                className = "suggestion-active";
                            }

                            return (
                                <li
                                    className={className}
                                    key={suggestion}
                                    onClick={onClick}
                                >
                                    {suggestion}
                                </li>
                            );
                        })}
                    </ul>
                );
            } else {
                suggestionsListComponent = (
                    <div class="no-suggestions">
                        <em>Nie znaleziono stoiska</em>
                    </div>
                );
            }
        }

        return (
            <>
                <input
                    type="text"
                    onChange={onChange}
                    onKeyDown={onKeyDown}
                    value={userInput}
                />
                {suggestionsListComponent}
            </>
        );
    }
}
export default Search
