import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getStand, updateStand, clearStand, deleteStand } from '../../actions'

class EditStand extends PureComponent {

    state = {
        formdata: {
            _id: this.props.match.params.id,
            name: '',
            x: '',
            y: '',
            informations: '',
        }
    }


    handleInput = (event, name) => {
        const newFormdata = {
            ...this.state.formdata
        }
        newFormdata[name] = event.target.value

        this.setState({
            formdata: newFormdata
        })
    }


    submitForm = (e) => {
        e.preventDefault();
        this.props.dispatch(updateStand(this.state.formdata))
    }

    deleteStand = () => {
        this.props.dispatch(deleteStand(this.props.match.params.id))
    }
    redirectUser = () => {
        setTimeout(() => {
            this.props.history.push('/user/user-stands')
        }, 500)
    }


    componentWillMount() {
        this.props.dispatch(getStand(this.props.match.params.id))
    }

    componentWillReceiveProps(nextProps) {
        let stand = nextProps.stands.stand;
        this.setState({
            formdata: {
                _id: stand._id,
                shortName: stand.shortName,
                name: stand.name,
                standSize: stand.size,
                x: stand.x,
                y: stand.y,
                informations: stand.informations,
            }
        })
    }

    componentWillUnmount() {
        this.props.dispatch(clearStand())
    }

    render() {
        let stands = this.props.stands;
        return (
            <div className="rl_container article">
                {
                    stands.updateStand ?
                        <div className="edit_confirm">
                            stand updated , <Link to={`/`}>
                                zobacz zmiany
                            </Link>
                        </div>
                        : null
                }
                {
                    stands.postDeleted ?
                        <div className="red_tag">
                            stand Deleted
                            {this.redirectUser()}
                        </div>
                        : null
                }

                <form onSubmit={this.submitForm}>
                    <h2>Edit stand</h2>

                    <div className="form_element">
                        <input
                            type="text"
                            placeholder="Enter shortName"
                            value={this.state.formdata.shortName}
                            onChange={(event) => this.handleInput(event, 'shortName')}
                        />
                    </div>

                    <div className="form_element">
                        <input
                            type="text"
                            placeholder="Enter name"
                            value={this.state.formdata.name}
                            onChange={(event) => this.handleInput(event, 'name')}
                        />
                    </div>

                    <div className="form_element">
                        <select name="size" placeholder="Wielkość stoiska" onChange={(event) => this.handleInput(event, 'size')}>
                            <option value="SMALL">małe</option>
                            <option value="MEDIUM">średnie</option>
                            <option value="BIG">duże</option>
                        </select>
                    </div>

                    <div className="form_element">
                        <input
                            type="number"
                            placeholder="Enter x"
                            value={this.state.formdata.x}
                            onChange={(event) => this.handleInput(event, 'x')}
                        />
                    </div>

                    <div className="form_element">
                        <input
                            type="number"
                            placeholder="Enter y"
                            value={this.state.formdata.y}
                            onChange={(event) => this.handleInput(event, 'y')}
                        />
                    </div>

                    <div className="form_element">
                        <input
                            type="TEXT"
                            placeholder="Enter informations"
                            value={this.state.formdata.informations}
                            onChange={(event) => this.handleInput(event, 'informations')}
                        />
                    </div>


                    <button type="submit">Edit review</button>
                    <div className="delete_stand">
                        <div className="button"
                            onClick={this.deleteStand}
                        >
                            Delete review
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        stands: state.stands
    }
}

export default connect(mapStateToProps)(EditStand)