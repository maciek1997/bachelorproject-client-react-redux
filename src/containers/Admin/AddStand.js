import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { clearNewStand, addStand } from '../../actions'

class AddStand extends Component {

    state = {
        formdata: {
            shortName: '',
            name: '',
            size: 'MEDIUM',
            x: '',
            y: '',
            informations: '',
        }
    }


    handleInput = (event, name) => {
        const newFormdata = {
            ...this.state.formdata
        }
        newFormdata[name] = event.target.value

        this.setState({
            formdata: newFormdata
        })
    }

    showNewStand = (stand) => (
        stand.post ?
            <div className="conf_link">
                <Link to={`/`}>
                    Dodano nowe stoisko
                </Link>
            </div>
            : null
    )

    submitForm = (e) => {
        e.preventDefault();
        this.props.dispatch(addStand({
            ...this.state.formdata,
            ownerId: this.props.user.login.id
        }))
    }

    componentWillUnmount() {
        this.props.dispatch(clearNewStand())
    }

    render() {
        return (
            <div className="rl_container article">
                <form onSubmit={this.submitForm}>
                    <h2>Add a Stand</h2>

                    <div className="form_element">
                        <input
                            type="text"
                            placeholder="skrócona nazwa stoiska"
                            value={this.state.formdata.shortName}
                            onChange={(event) => this.handleInput(event, 'shortName')}
                        />
                    </div>

                    <div className="form_element">
                        <input
                            type="text"
                            placeholder="Nazwa stoiska"
                            value={this.state.formdata.name}
                            onChange={(event) => this.handleInput(event, 'name')}
                        />
                    </div>

                    <div className="form_element">
                        <select name="size" defaultValue="MEDIUM" placeholder="Wielkość stoiska" onChange={(event) => this.handleInput(event, 'size')}>
                            <option value="SMALL">małe</option>
                            <option value="MEDIUM">średnie</option>
                            <option value="BIG">duże</option>
                        </select>
                    </div>

                    <div className="form_element">
                        <input
                            type="number"
                            placeholder="Współrzędne X"
                            value={this.state.formdata.x}
                            onChange={(event) => this.handleInput(event, 'x')}
                        />
                    </div>

                    <div className="form_element">
                        <input
                            type="number"
                            placeholder="Współrzędne Y"
                            value={this.state.formdata.y}
                            onChange={(event) => this.handleInput(event, 'y')}
                        />
                    </div>

                    <div className="form_element">
                        <input
                            type="TEXT"
                            placeholder="Enter informations"
                            value={this.state.formdata.informations}
                            onChange={(event) => this.handleInput(event, 'informations')}
                        />
                    </div>


                    <button type="submit">Add Stand</button>
                    {
                        this.props.stands.newStand ?
                            this.showNewStand(this.props.stands.newStand)
                            : null
                    }
                </form>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        stands: state.stands
    }
}

export default connect(mapStateToProps)(AddStand)