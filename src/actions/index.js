import axios from 'axios';

export function getStands(
    limit = 30,
    start = 0,
    order = 'asc',
    list = '',
    name = ''
) {
    const request = axios.get(`/api/allStands?skip=${start}&limit=${limit}&order=${order}&name=${name}`)
        .then(response => {
            if (list) {
                return [...list, ...response.data]
            } else {
                return response.data
            }
        })
    return {
        type: 'GET_STANDS',
        payload: request
    }
}

export function getStand(id) {
    const request = axios.get(`/api/getStand?id=${id}`)
        .then(response => response.data);

    return {
        type: 'GET_STAND',
        payload: request
    }
}


export function addStand(stand) {
    const request = axios.post('/api/stand', stand)
        .then(response => response.data);

    return {
        type: 'ADD_STAND',
        payload: request
    }
}

export function clearNewStand() {
    return {
        type: 'CLEAR_NEWSTAND',
        payload: {}
    }
}

export function getStandWithAuthor(id) {
    const request = axios.get(`/api/getStand?id=${id}`)

    return (dispatch) => {
        request.then(({ data }) => {
            let stand = data;

            axios.get(`/api/getReviewer?id=${stand.ownerId}`)
                .then(({ data }) => {
                    let response = {
                        stand,
                        reviewer: data
                    }

                    dispatch({
                        type: 'GET_STAND_W_REVIEWER',
                        payload: response
                    })
                })
        })
    }
}

export function clearStandWithReviewer() {
    return {
        type: 'CLEAR_STAND_W_REVIEWER',
        payload: {
            stand: {},
            reviewer: {}
        }
    }
}

export function getUserStands(userId) {
    const request = axios.get(`/api/user_stands?user=${userId}`)
        .then(response => response.data)

    return {
        type: 'GET_USER_STANDS',
        payload: request
    }
}

export function deleteStand(id) {
    const request = axios.delete(`/api/delete_stand?id=${id}`)
        .then(response => response.data)

    return {
        type: 'DELETE_STAND',
        payload: request
    }
}

export function updateStand(data) {
    const request = axios.post(`/api/stand_update`, data)
        .then(response => response.data);

    return {
        type: 'UPDATE_STAND',
        payload: request
    }

}

export function clearStand() {
    return {
        type: 'CLEAR_STAND',
        payload: {
            stand: null,
            updateStand: false,
            postDeleted: false
        }
    }
}

/*========= USER ===========*/

export function loginUser({ email, password }) {
    const request = axios.post('/api/login', { email, password })
        .then(response => response.data)

    return {
        type: 'USER_LOGIN',
        payload: request
    }
}

export function auth() {
    const request = axios.get('/api/auth')
        .then(response => response.data);

    return {
        type: 'USER_AUTH',
        payload: request
    }

}


export function getUsers() {
    const request = axios.get(`/api/users`)
        .then(response => response.data);

    return {
        type: 'GET_USER',
        payload: request
    }
}


export function userRegister(user, userList) {
    const request = axios.post(`/api/register`, user)

    return (dispatch) => {
        request.then(({ data }) => {
            let users = data.success ? [...userList, data.user] : userList;
            let response = {
                success: data.success,
                users
            }

            dispatch({
                type: 'USER_REGISTER',
                payload: response
            })
        })
    }
}