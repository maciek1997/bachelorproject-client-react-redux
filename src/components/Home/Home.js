import React from 'react';
import { connect } from 'react-redux';
import { getStands } from '../../actions';
import * as d3 from 'd3';
import Search from '../../containers/Search';

class Home extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            inputValue: '',
            list: [],
            active: {},
            SVGwidth: 650,
            SVGheight: 440,
        }

        this.drawSVG = this.drawSVG.bind(this);
        this.selectedStand = this.selectedStand.bind(this);
        this.writePath = this.writePath.bind(this);
        this.countCloserPoint = this.countFirstStep.bind(this);
    }



    async componentDidMount() {
        await this.props.dispatch(getStands());
        this.setState({
            list: this.props.stands.list
        })
        this.drawSVG();

        document.addEventListener('click', (e) => {
            if (!e.target.closest("rect")) {
                d3.selectAll("rect").attr("fill", "white");
                this.setState({
                    active: {}
                });
                d3.select("path").remove();

            }
        })

    }


    // metoda sprawdzająca, który punkt jest najbliższy
    countFirstStep(startPoint, endPoint) {
        // współrzędne y początkowe (yp)
        // współrzędne x końcowe (xk), potrzebne do wzoru na obliczenie odległości pomiędzy dwoma punktami (z pitagorasa)
        const yp = startPoint.y;
        const xk = endPoint.x;

        // trzy statyczne punkty, które wyznaczają pierwszą drogę jaka zostanie wyświetlona na ekranie 
        const firstPoint = { "x": 260, "y": 390 };
        const secondPoint = { "x": 380, "y": 390 };
        const thirdPoint = { "x": 560, "y": 390 };

        // obliczenie odległości każdego ze statycznych punktów oraz punktu początkowego oraz końcowego
        const firstPointer = Math.sqrt((firstPoint.x - xk) * (firstPoint.x - xk) + (yp - firstPoint.y) * (yp - firstPoint.y));
        const secondPointer = Math.sqrt((secondPoint.x - xk) * (secondPoint.x - xk) + (yp - secondPoint.y) * (yp - secondPoint.y));
        const thirdPointer = Math.sqrt((thirdPoint.x - xk) * (thirdPoint.x - xk) + (yp - thirdPoint.y) * (yp - thirdPoint.y));

        // zwracany jest punkt, który ma najmniejszy pierwiastek
        if (firstPointer < secondPointer && firstPointer < thirdPointer) {
            return firstPoint;
        } else if (secondPointer < firstPointer && secondPointer < thirdPointer) {
            return secondPoint
        } else if (thirdPointer < firstPointer && thirdPointer < secondPointer) {
            return thirdPoint
        } else return firstPoint;
    }

    // metoda obliczająca przed ostatni krok 
    countSecondStep(firstPoint, thirdPoint) {

        const xp = firstPoint.x;
        const yk = thirdPoint.y;

        // dodanie korygującej wartości osi Y by wykres ładnie wyglądał.
        // let addedCoordinate = this.standLength(this.state.active) / 2;


        return { x: xp, y: yk }
    }

    standLength(stand) {
        if (stand.size === "SMALL") { return 50; }
        if (stand.size === "MEDIUM") { return 70; }
        if (stand.size === "BIG") { return 100; }
    }


    writePath() {
        d3.select("path").remove();

        let addedCoordinate = this.standLength(this.state.active) / 2;

        const startPoint = { "x": 260, "y": 430 };
        const endPoint = {
            "x": this.state.active.x + addedCoordinate,
            "y": this.state.active.y + addedCoordinate
        }
        const firstStep = this.countFirstStep(startPoint, endPoint);

        const thirdStep = {
            "x": this.state.active.x + addedCoordinate,
            "y": this.state.active.y + addedCoordinate * 2 + 10
        }
        const secondStep = this.countSecondStep(firstStep, thirdStep);


        let lineData = [
            startPoint,
            firstStep,
            secondStep,
            thirdStep,
            endPoint
        ];

        let lineFunction = d3.line()
            .x(function (d) { return d.x; })
            .y(function (d) { return d.y; })

        d3.select("svg").append('path').attr("d", lineFunction(lineData))
            .attr("stroke", "blue")
            .attr("stroke-width", 1)
            .attr("fill", "none");


    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextState.active === this.state.active) {
            return false;
        }
        return true;
    }

    drawSVG() {
        const list = this.props.stands.list;
        const w = this.state.SVGwidth;
        const h = this.state.SVGheight;

        const svg = d3.select("#SVG").append("svg")
            .attr("width", w).attr("height", h)
            .attr("style", "outline: thin solid gray;")
            .attr("padding", "50px")

        const rectangles = svg.selectAll("rect")
            .data(list)
            .enter()
            .append("g")

        rectangles.append("rect")
            .attr("id", (d, i) => d._id)
            .attr("x", (d, i) => d.x)
            .attr("y", (d, i) => d.y)
            .attr("width", (d, i) => this.standLength(d))
            .attr("height", (d, i) => this.standLength(d))
            .attr("fill", "white")
            .attr('stroke', 'gray')
            .on("click", handleMouseClick)

        rectangles.append('text')
            .attr("x", (d, i) => d.x + 3)
            .attr("y", (d, i) => d.y + 3)
            .text((d, i) => d.name)
            .attr("font-family", "sans-serif")
            .attr("font-size", "12px")
            .call(wrap);


        svg.append("rect")
            .attr("x", 245)
            .attr("y", h - 10)
            .attr("width", 30)
            .attr("height", 10)
            .attr("fill", "black")
            .attr('stroke', 'gray')
        svg.append("text")
            .attr("x", 250 - 40)
            .attr("y", h - 15)
            .text("wejście")
            .attr("font-family", "sans-serif")
            .attr("font-size", "12px")



        function wrap(text) {
            text.each(function () {
                let text = d3.select(this),
                    words = text.text().split(/\s+/).reverse(),
                    word,
                    line = [],
                    lineNumber = 0,
                    lineHeight = 1.1, // ems
                    x = text.attr("x"),
                    y = text.attr("y"),
                    tspan = text.text(null).append("tspan").attr("x", x).attr("y", y)
                while (word = words.pop()) {
                    line.push(word);
                    tspan.text(line.join(" "));
                    if (tspan.node().getComputedTextLength()) {
                        line.pop();
                        tspan.text(line.join(" "));
                        line = [word];
                        tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + "em").text(word);
                    }
                }
            });
        }

        const self = this;
        function handleMouseClick(d, i) {
            if (d._id === self.state.active) {
                return;
            } else {
                self.setState({
                    active: d
                }, () => {
                    d3.selectAll("rect").attr("fill", "white");
                    d3.select("rect[id='" + d._id + "']").attr("fill", "lightgray");
                    self.writePath();

                })
            }
        }
    }

    selectedStand = (stand) => {
        this.setState({
            active: stand
        }, () => {
            d3.selectAll("rect").attr("fill", "white");
            d3.select("rect[id='" + stand._id + "']").attr("fill", "lightgray");
            this.writePath();

        })
    }

    render() {
        const { active } = this.state;
        return (
            <div className="row ">
                <div className="col-xs-8 SVG-container" id="SVG"></div>
                <div className="col-xs-4 padding-top-60">
                    <Search method={this.selectedStand} suggestions={this.state.list} />
                    <div className="padding-top-60">
                        {
                            active.name && <>
                                <div>Pełna nazwa: {active.name}</div>
                                <div>Informacje: {active.informations}</div>
                                <div>Id: {active.ownerId}</div>
                            </>
                        }
                    </div>
                </div>
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        stands: state.stands
    }
}

export default connect(mapStateToProps)(Home)