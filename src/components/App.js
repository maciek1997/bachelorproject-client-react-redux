import React from 'react';
import { connect } from 'react-redux';
import { getStands } from '../actions';
import * as d3 from 'd3';

class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            inputValue: '',
            list: []
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.renderAutocomplete = this.renderAutocomplete.bind(this);
    }

    async componentDidMount() {
        await this.props.dispatch(getStands(25, 0, 'desc'));
        this.setState({
            list: this.props.stands.list
        })
        this.drawChart();
    }

    drawChart() {
        // console.log(this.props.stands && this.props.stands.list);
        // let XandY = {}
        const list = this.props.stands.list;
        // const data = [12, 5, 6, 6, 9, 10];
        const w = 900;
        const h = 600;
        const svg = d3.select("#Chart")
            .append("svg")
            .attr("width", w)
            .attr("height", h);

        svg.selectAll("rect")
            .data(list)
            .enter()
            .append("rect")
            .attr("id", (d, i) => list[i]._id)
            .attr("x", (d, i) => list[i].x)
            .attr("y", (d, i) => list[i].y)
            .attr("width", 140)
            .attr("height", 70)
            .attr("fill", "blue");
    }

    async handleInputChange(e) {
        await this.setState({
            inputValue: e.target.value
        })
        const stateList = this.state.list;
        const newList = [];
        stateList.forEach(item => {
            console.log(item.name.includes(this.state.inputValue));

            return item.name.includes(this.state.inputValue) ? newList.push(item) : null

        }
        )

        this.setState({
            list: newList
        })

    }

    renderAutocomplete() {
        this.state.list.map((item, index) => {
            return (
                <div key={index}>
                    {item._id}
                </div>
            )
        })
    }

    render() {
        return (
            <div>

                <div id="barChart"></div>

                <input type="text"
                    autoComplete="off"
                    className="react-autosuggest__input"
                    placeholder="stoisko..."
                    value={this.state.inputValue}
                    onChange={this.handleInputChange}
                />
                <div>
                    {
                        this.state.list && this.state.list.map((item, index) => {
                            return (
                                <div key={index}>
                                    hello
                                    {/* {item} */}
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        stands: state.stands
    }
}
export default connect(mapStateToProps)(App);