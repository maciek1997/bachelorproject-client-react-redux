import React from 'react';

const SingleStand = ({ data }) => {
    console.log(data);
    // const rectangle = "hello";
    const rectangle = ({ standId, name, properties, x: x = 25, y: y = 25 }) => (
        <Rectangle key={standId} x={x} y={y} />
    )

    return (
        <g className="stands">
            {
                rectangle(data)
            }
        </g>
    )
}
const Rectangle = ({ x, y }) => {
    console.log(x, y);
    return (
        <rect width={x} height={y} />
    )
}


export default SingleStand;