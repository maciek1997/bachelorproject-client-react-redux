import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUserStands } from '../../actions';
import moment from 'moment-js';
import { Link } from 'react-router-dom';

class UserStands extends Component {

    componentWillMount() {
        console.log(this.props);
        this.props.dispatch(getUserStands(this.props.user.login.id))
    }

    showUserStands = (user) => (
        user.userStands ?
            user.userStands.map(item => (
                <tr key={item._id}>
                    <td>
                        <Link to={`/user/edit-stand/${item._id}`}>
                            {item.name}
                        </Link>
                    </td>

                    <td>{item._id}</td>

                    <td>
                        {moment(item.createAt).format("MM/DD/YY")}
                    </td>
                </tr>
            ))
            : null
    )

    render() {
        let user = this.props.user;
        console.log(user);
        return (
            <div className="user_stands">
                <h4>Your reviews:</h4>
                <table>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>OwnerId</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.showUserStands(user)}
                    </tbody>
                </table>
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(UserStands)