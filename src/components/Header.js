import React from 'react';
import { Link } from 'react-router-dom';
import Nav from './Header/Sidenav';
import FontAwesome from 'react-fontawesome';

class Header extends React.Component {


    state = {
        showNav: false
    }

    onHideNav = () => {
        this.setState({ showNav: false })
    }


    render() {
        return (
            <header>
                <div className="open_nav">
                    <FontAwesome name="bars"
                        onClick={() => this.setState({ showNav: true })}
                        style={{
                            color: '#ffffff',
                            padding: '10px',
                            cursor: 'pointer'
                        }}
                    />
                </div>
                <Nav
                    showNav={this.state.showNav}
                    onHideNav={() => this.onHideNav()}
                />

                <Link to="/" className="logo">
                    Stoiska
            </Link>

            </header>
        );
    }
}

export default Header;