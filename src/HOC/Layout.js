import React from 'react';
import Header from '../components/Header';

class Layout extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Layout;