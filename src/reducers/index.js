import { combineReducers } from 'redux';
import stands from './stand_reducer';
import user from './user_reducer';

const rootReducer = combineReducers({
    stands,
    user
})

export default rootReducer;