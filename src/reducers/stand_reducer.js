export default function (state = {}, action) {
    switch (action.type) {
        case 'GET_STANDS':
            return { ...state, list: action.payload }
        case 'GET_STAND':
            return { ...state, stand: action.payload }
        case 'GET_STAND_W_REVIEWER':
            return {
                ...state,
                stand: action.payload.STAND,
                reviewer: action.payload.reviewer
            }
        case 'CLEAR_STAND_W_REVIEWER':
            return {
                ...state,
                stand: action.payload.stand,
                reviewer: action.payload.reviewer
            }
        case 'ADD_STAND':
            return { ...state, newStand: action.payload }
        case 'CLEAR_NEWSTAND':
            return { ...state, newStand: action.payload }
        case 'UPDATE_STAND':
            return {
                ...state,
                updateStand: action.payload.success,
                stand: action.payload.doc
            }
        case 'DELETE_STAND':
            return {
                ...state,
                postDeleted: action.payload
            }
        case 'CLEAR_STAND':
            return {
                ...state,
                updateStand: action.payload.updateStand,
                stand: action.payload.stand,
                postDeleted: action.payload.postDeleted
            }
        default:
            return state;
    }
}